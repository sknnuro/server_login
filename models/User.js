const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
  fname: {
    type: String,
  },
  lname: {
    type: String,
  },
  email: {
    type: String,
  },
  password: {
    type: String,
  },
  number: {
    type: String,
  },
  dob: {
    type: Date,
  },
  pob: {
    type: String
  },
  nationality: {
    type: String,
  },
  pofemployment: {
    type: String,
  },
  profession: {
    type: String,
  },
  raffi: {
    type: String,
  },
  caffi: {
    type: String,
  },
  sattended: {
    type: String,
  },
  yeargraduated: {
    type: Date,
  },
  degreeobtained: {
    type: String
  },
  training: {
    type: String
  },
  mstatus: {
    type: String
  },
  fathern: {
    type: String
  },
  fatherage: {
    type: String
  },
  
  occupation: {
    type: String
  },
  church: {
    type: String
  },
 
  mothersname: {
    type: String
  },
  mothersage: {
    type: String
  },
  churcha: {
    type: String
  },
  occup: {
    type: String
  },
  once1: {
    type: String
  },
  options: {
    type: String
  },
  list: {
    type: String,
    
  },
  children: {
    type: String,
    
  },
  medication: {
    type: String,
    
  },
  hospital: {
    type: String,
    
  },
understand: {
    type: String,
    
  },
  rites: {
    type: String,
    
  },
  fiance: {
    type: String,
    
  },
  together: {
    type: String,
    
  },
  live: {
    type: String,
    
  },
  marriage: {
    type: String,
    
  },
  mean: {
    type: String,
    
  },
  describe: {
    type: String,
    
  },
  different: {
    type: String,
    
  },
  dateC: {
    type: Date,
    
  },
  otp : {
    type : String
  },
  otpUsed:{
type:Boolean,
default:false
  },

  dateCreated:{
    type:Date,default:Date.now
  }
  
});


const User=mongoose.model('User',UserSchema);
module.exports = User;
