const mongoose = require('mongoose')

const schema = new mongoose.Schema({
    UserId:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'User'
    },
    results:{
        type: Object,
        required: true,
        default:{}
    },
    dateCreated:{
    type : Date,
    required : true,
    default:Date.now
    }

})

const model=mongoose.model('Results',schema);
module.exports=model;