const express = require('express');
const expressLayouts = require('express-ejs-layouts');
const mongoose = require('mongoose');
const passport = require('passport');
const flash = require('connect-flash');
const session = require('express-session');
const nodemailer = require('nodemailer')
var cors = require('cors');
const app = express();
const bodyParser = require('body-parser')
const randomize = require("randomatic");
app.use(cors());


  




// Passport Config
require('./config/passport')(passport);

app.use(bodyParser.json())


// DB Config
const db = require('./config/keys').mongoURI;
const User = require('./models/User');
const Results= require('./models/results');

// Connect to MongoDB
mongoose
  .connect(
    db,
    { useNewUrlParser: true ,useUnifiedTopology: true}
  )
  .then(() => console.log('MongoDB Connected'))
  .catch(err => console.log(err));

// Express session
app.use(
  session({
    secret: 'secret',
    resave: true,
    saveUninitialized: true
  })
);

// Passport middleware
app.use(passport.initialize());
app.use(passport.session());

// Connect flash
app.use(flash());


// Routes


app.post('/api/login',async(req,res)=>{
  try {
    const{email,password} = req.body

  console.log(email,password)

  const result= await User.findOne({email,password})
  if(!result){
    res.json({
      success:false,
      message:"Incorrect details"
    })
  }else{
    res.json({
      success:true,
      message:"logged in"
  })
  console.log("logging you in")
}
  } catch (error) {
    
  }
  
})


app.post("/api/verify",async(req,res)=>{   
  try {
    const {otp} = req.body;
    const result= await User.findOne({otp:otp});
    if(!result|| !result._id){
    throw Error("Oops somehting went wrong")
    
  }
    return res.json({
      success:true,
      data:otp
    }
  )
  } catch (err) {
    return res.json({
      success:false,
      message:err.message
    }
    )
    
  }
 
})


app.get('/api/summary/get',async(request,res)=>{
  try {
    const [count,results]=await Promise.all(
      [
        User.count({}),
        Results.count({})
      ]
    )
    return res.json(
      {
        success:true,
        message:'success',
        data:{
          users:count,
          results:results
        }
      }
    )
  } catch (err) {
    res.json(
      {
        success:false,
        message:err.message
      }
    )
  }
})

     
app.post("/api/submit",async(req,res)=>{
  try {
    const {email,results} = req.body;
    if(!email){
      throw Error(
        'Invalid request'
      )
    }
    const user =await User.findOneAndUpdate({
      email:email
    },{ otpUsed:true });
    if(!user || !user._id){
      throw Error(
        'User account is not in our system'
      )
    }
    const response = await Results.create(
      {
        UserId:user['_id'],
        results:results
      }
    );
    if(!response|| !response._id){
      throw Error(
        'Oops something went wrong'
      )
    }
    return res.json(
      {
        success:true,
        data:results
      }
    )
  } catch (err) {
    return res.json(
      {
        success:false,
        message:err.message
      }
    )
  }

 })


app.post('/api/register',async(req,res) =>{
  try {
    const {email} = req.body;
    const existingUser = await User.findOne({email:email});
      if(existingUser && existingUser._id){
        throw Error(
          'Email is already in use'
        )
      }
      let random = randomize("0", 6);
      const data={...req.body,otp:random,otpUsed:false,dateCreated:Date.now()};
      const user =await User.create(data);
      return res.json(
        {
          success:true,
          data:user
        }
      )
  } catch (err) {
    console.log(err);
    return res.json(
      {
        success:false,
        message:err.message
      }
    )
  }
})

app.get('/api/users/get', async(req,res)=>{
  try {
    const users = await User.find({});

    return res.json(
      {
        success:true,
        data:users
      }
    )
  } catch (err) {
    return res.json(
      {
        success:false,
        message:err.message
      }
    )
  }
});



app.get('/api/results/get', async(req,res)=>{
  try {
    const users=await Results.find({}).populate('UserId');
    return res.json(
      {
        success:true,
        data:users
      }
    )
  } catch (err) {
    return res.json(
      {
        success:false,
        message:err.message
      }
    )
  }
});


// app.use('/', require('./routes/index.js'));
// app.use('/users', require('./routes/users.js'));

app.listen(5000,()=> console.log('Server listening at 5000'))
